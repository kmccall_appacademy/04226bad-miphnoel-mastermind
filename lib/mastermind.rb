class Code
  attr_reader :pegs

  def initialize(pegs)
    @pegs = pegs
  end


  def self.random
    secret_code = []

    4.times do
      secret_code << PEGS.keys.sample
    end

    Code.new(secret_code)
  end

  def self.parse(string)
    raise "invalid code length" unless string.length == 4
    input_code = Code.new(string.downcase.split(''))
    raise "invalid color" unless input_code.pegs.all? do |peg|
       PEGS.keys.include?(peg)
    end

    input_code
  end

  PEGS = {
      'r' => "Red",
      'g' => "Green",
      'b' => "Blue",
      'y' => "Yellow",
      'o' => "Orange",
      'p' => "Purple"
    }

  def [](peg_index)
    pegs[peg_index]
  end

  def exact_matches(code2)
    matches = 0

    pegs.each_index { |idx| matches += 1 if self[idx] == code2[idx] }

    matches
  end

  def near_matches(code2)
    count_hash1, count_hash2 = count_hash, code2.count_hash
    matches = 0

    count_hash2.each do |peg, count|
      matches += [count, count_hash1[peg]].min
    end

    matches - exact_matches(code2)
  end

  def ==(code2)
    code2.class == Code && pegs == code2.pegs
  end

  def count_hash
    count = Hash.new(0)

    pegs.each { |peg| count[peg] += 1 }

    count
  end

end

class Game
  attr_reader :secret_code, :guesses_remaining, :guess

  def initialize(code=Code.random)
    @secret_code = code
    @guesses_remaining = 10
    @guess = nil
  end

  def play
    intro
    take_turn until over?
    outro
  end

  def get_guess
    print "Guess the secret code: "
    Code.parse(gets.chomp)
  rescue
    guess_again
  end

  def display_matches(code)
    puts "\nexact matches: #{secret_code.exact_matches(code)}"
    puts "near matches: #{secret_code.near_matches(code)}"
  end

  private

  def over?
    guesses_remaining.zero? || guessed?
  end

  def guessed?
    secret_code == guess
  end

  def take_turn
    puts "\nGuesses remaining: #{guesses_remaining}"
    @guess = get_guess
    display_matches(guess)
    @guesses_remaining -= 1
  end

  def guess_again
    puts "\nPlease guess in the form of the first letter of FOUR colors:"
    puts "Valid guesses: r, g, b, y, o, or p"
    puts "Valid format: rgby, oopp, ygyr etc."
    get_guess
  end

  def intro
    puts "It's time for Mastermind!"
    puts "I have created a code consisting of FOUR colors:"
    puts "Red, Green, Blue, Yellow, Orange, Purple"
    puts "You have 10 tries to guess the secret code"
    puts "by typing the first letter of four colors (ex. rybg)."
    puts "I will tell you how many of your colors are in my secret code"
    puts "and how many are already in the right positions!"
    puts "The colors CAN repeat"
    puts "Good luck!"
  end

  def outro
    if guessed?
      puts "You got it! You're a true Mastermind."
    else
      puts "I win this time! My secret code was #{secret_code.pegs.join}"
    end
  end
end

# if __FILE__ == $PROGRAM_NAME
#   game = Game.new
#   game.play
# end
